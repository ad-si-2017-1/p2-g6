// Importa as configurações do servidor
var app = require('./config/server');
var irc =  require('irc'); // importa irc

// Define a porta de escuta
var server = app.listen(3000, function(){
	console.log('Servidor online na porta 3000');
})

var io = require('socket.io').listen(server);

app.set('io', io);


// Criando a conexão por websocket
io.on('connection', function(socket){

	console.log('Usuário conectou');
	var cliente = new irc.Client('irc.freenode.net', "",{ autoConnect: false});
	
	socket.on('conectandoServidor', function(data){
		cliente = new irc.Client('irc.freenode.net', data.nick,{ autoConnect: false});

		cliente.connect(20, function(nick){
			console.log('Metsu');
		
			cliente.join(data.canais, function (nick) {
				cliente.say(data.canais, "HADOOOUKEN");
			});

			//Emite mensagem que se connectou
			socket.emit('msgDoServer', {
				nick: '<strong>Server</strong>',
				canal: "",
				mensagem: 'Você se connectou ao Canal: '+data.canais,
				hora: hora()
			});// fim da mensagem se connectado
		});

		//Escuta mensagem do server
		cliente.addListener('message', function (from, to, text) {
			socket.emit('msgDoServer',
			{
				nick: from,
				canal: to,
				mensagem: text,
				hora: hora()
			});
		});// fim escuta mensagem do server

		socket.on('entrarCanal', function(data){
			cliente.join(data.canais, 'Cheguei Chegando!');

			socket.emit('msgDoServer', {
				nick: '<strong>Server</strong>',
				canal: "",
				mensagem: 'Você se connectou ao Canal: '+data.canais,
				hora: hora()
			});
		});

		socket.on('partCanal', function(data){
			if(!data.mensagem) data.mensagem = 'Saiu do canal';
			cliente.part(data.canais, data.mensagem);
				socket.emit('msgDoServer', {
				nick: '<strong>Server</strong>',
				canal: "",
				mensagem: 'Voce saiu do canal '+ data.canais,
				hora: hora()
			});
		});

		cliente.addListener('names', function (canaisy, nomes) {
			socket.emit('nomesCanal', {
				nome: nomes,
				canais: canaisy
			});
			// console.log("Canal: "+ canaisy);
			// console.log("Nicks: " + nomes);
		});

		socket.on('informaCanalDoProxy', function(data){
			socket.broadcast.emit('novoCanalDoProxy', {
				canal: data.canal,
				nome: data.nome
			});
		});

	});// Fim Conexao Servidor irc

	
	socket.on('disconnect', function(data){
		console.log('Usuário desconectou ');
	});



	//Registrando os canais e emitindo os seus respectivos nomes
	socket.on('registraCanal', function(data){
		socket.emit('enviaCanal',{canais: data.canais});
	});




	socket.on('enviaMsgServ', function(data){
		cliente.say(data.canais, data.mensagem);
		socket.emit(
			'msgDoServer',
			{nick: data.nick, mensagem: data.mensagem, hora: hora(),canal: data.canais}
			);
		socket.broadcast.emit(
			'msgDoServer',
			{nick: data.nick, mensagem: data.mensagem, hora: hora(), canal:data.canais}
			);
	});


});

function hora(){
	var d = new Date();
  	var h = d.getHours();
  	var m = d.getMinutes();
  	var s = d.getSeconds();
  	var hora = h+":"+m+":"+s;
	return hora
}

app.get('/', function(req , res){
	res.render('index',{validacao: {}});
});

app.post('/chat', function(req , res){
	var login = req.body;

	req.assert('nick', 'Nick é obrigatorio').notEmpty();
	req.assert('nick', 'Nick precisa ter de 3 a 15 caracteres').len(3, 15);

	req.assert('canais','Você deve escolher um canal para entrar no server').notEmpty();


	var erros = req.validationErrors(); 

	if(erros){
		res.render("index", {validacao : erros});
		// res.send('Existem erros');
		return;
	}
	
	io.emit('conexao', {nick: login.nick, mensagem: 'Entrou no chat', hora: hora()});
	

	res.render('chat', {login: login});
});