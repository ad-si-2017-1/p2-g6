
var socket = io('http://localhost:3000');
var canais = {};
$("#enviarMsg").click(function(){
	enviarMsg();
});

$('#inputMsg').on('keydown', function (event) { 
    if (event.keyCode === 13) {
        enviarMsg();
    }
});

socket.on('msgDoServer', function(data){
	var msg ="";
	msg += "<h3>"+ data.nick +"  "+ data.canal +" <small> ["+ data.hora+"]</h3>";
	msg += "<p class='showMsg'>" + data.mensagem +'</p>';
	$('#boxMsg').append(msg);
});

socket.on('conexao', function(data){
	var msg="";
	msg += "<h3>"+ data.nick +" <small> ["+ data.hora+"]</h3>";
	msg += "<p class='showMsg'>" + data.mensagem +'</p>';

	$('#boxMsg').append(msg);
});

//Inserindo as informações obtidas no id da div
socket.on('enviaCanal', function(data){
	var regCanal="";
	regCanal += "<a href='javascript:joinsala("+data.canais+")' ><p id='linkCanal'>"+ data.canais + "</p></a>";
	$('#jurema').append(regCanal);
});


socket.on('nomesCanal', function(data){
	var msg ="";
	for (var nome in data.nome) {
		msg +='<p>' + nome + '</p>';
	}
	$('#jurema').append(msg);

	socket.emit('informaCanalDoProxy', {
		canal: data.canais,
		nomes: data.nome
	});
});

socket.on('novoCanalDoProxy', function(data){
	var msg ="";
	for (var nome in data.nome) {
		msg +='<p>' + nome + '</p>';
	}

	$('#jurema').append("<a href="+'javascript:joinsala("'+ data.canal +'")'+"><p id='linkCanal'>" +data.canal + "</p></a>");
	$('#jurema').append(msg);
});


$(document).ready(function(){
	socket.emit(
		'conectandoServidor', 
		{
			nick: $('#nick').val(), 
			canais: $('#canais').val()
		}
		);
	//Função para inserir nome de canal na lista de canais disponíveis.
	// $('#jurema').append("<a href='#' ><p id='linkCanal'>"+ $('#canais').val() + "</p></a>");
	var CANAL = $('#canais').val()

	$('#jurema').append("<a href="+'javascript:joinsala("'+ CANAL +'")'+
		"><p id='linkCanal'>" +
		$('#canais').val() + 
		"</p></a>");
});
function joinsala(canal){
	socket.emit('entrarCanal', {canais: canal});
	$("#canais").val(canal);
	$('#topBar').text(canal);
}

function enviarMsg(){

	var mensagem = $('#inputMsg').val();
	if(mensagem.charAt(0) === '/'){
		var msgs = mensagem.split(" ");
		var cmd = msgs[0].toUpperCase();
		switch(cmd){
			case '/QUIT':
				socket.disconnect();
				window.location.replace("http://localhost:3000");
			break;

			case '/JOIN':
				if(msgs[1]){
					// socket.emit('entrarCanal', {canais: msgs[1]})
					$('#jurema').append("<a href="+'javascript:joinsala("'+ msgs[1] +'")'+"><p id='linkCanal'>" +msgs[1] + "</p></a>");
					joinsala(msgs[1]);
				}else{
					socket.emit('enviaMsgServ', {
						nick: '<strong>Server</strong>', 
						canal: $('#canais').val(),
						mensagem: 'Servidor Invalido'
					});
				}	
			break;

			case '/PART':
				if(!msgs[1]) alert('ta de brincation');
				socket.emit('partCanal', {canais: msgs[1]});
			break;

			default:
				alert('nofaa'+msgs[0]+ " :::"+ msgs[1]);
			break
		}
		
		$('#inputMsg').val("").focus();
		return;
	}

	socket.emit(
		'enviaMsgServ',
		{
			nick: $('#nick').val(), 
			canais: $('#canais').val(),
			mensagem: $('#inputMsg').val()
		}
		);
	$('#inputMsg').val("").focus();
	// $('#inputMsg').focus();

}