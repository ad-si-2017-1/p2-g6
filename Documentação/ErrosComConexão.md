At� o momento o software est� funcionando corretamente quando est� sem 
conex�o com a rede, por�m ao rodar o chat e se conectar na internet, algumas vezes � exibido um erro
com a biblioteca irc, o erro ocorre em momentos aleat�rios.

Segue o erro abaixo: 

#Erro apresentado no servidor
[nodemon] starting `node app`
Servidor online
Usu�rio conectou
Usu�rio conectou
c:\Git\p2-g6\node_modules\irc\lib\irc.js:849
                        throw err;
                        ^

Error: Uncaught, unspecified "error" event. ([object Object])
    at Client.emit (events.js:163:17)
    at Client.<anonymous> (c:\Git\p2-g6\node_modules\irc\lib\irc.js:643:26)
    at emitOne (events.js:96:13)
    at Client.emit (events.js:188:7)
    at iterator (c:\Git\p2-g6\node_modules\irc\lib\irc.js:846:26)
    at Array.forEach (native)
    at Socket.handleData (c:\Git\p2-g6\node_modules\irc\lib\irc.js:841:15)
    at emitOne (events.js:96:13)
    at Socket.emit (events.js:188:7)
    at readableAddChunk (_stream_readable.js:176:18)
    at Socket.Readable.push (_stream_readable.js:134:10)
    at TCP.onread (net.js:551:20)
[nodemon] app crashed - waiting for file changes before starting...


#Erro apresentado no console do cliente
Failed to load resource: socket.io/?EIO=3&transport=polling&t=LlgZt6x
net::ERR_CONNECTION_REFUSED


Suspeitamos que pode se tratar de um erro devido as inconst�ncias da rede, 
quando rodamos o chat ele cai e quando tenta se conectar novamente
ele n�o sabe o que fazer, pois ainda n�o temos instru��es implementadas,
ent�o � exibido o erro acima.
Para tentar sanar o erro, criaremos uma fun��o que se reconecta toda vez
que a conex�o for perdida, e tamb�m criaremos um tratamento de erros, para que
quando for desconectado da rede se exiba uma mensagem apropriada.